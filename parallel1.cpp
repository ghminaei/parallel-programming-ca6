#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <omp.h>
#include <Windows.h>



#define N  15

int solutions;

int put(int Queens[], int row, int column)
{
    int i;
    for (i = 0; i < row; i++) {
        if (Queens[i] == column || abs(Queens[i] - column) == (row - i))
            return -1;
    }
    Queens[row] = column;
    if (row == N - 1) {
        #pragma omp atomic
            solutions++;
    }
    else {
        
        for (i = 0; i < N; i++) { // increment row
            put(Queens, row + 1, i);
        }
    }
    return 0;
}


void solve(int Queens[]) {
    int i;
    #pragma omp parallel for num_threads(6) schedule(dynamic)
        for (i = 0; i < N; i++) {
            put(new int[N], 0, i);
        }
}



int main()
{
    int Queens[N];
    unsigned long start_time, diff;
    unsigned long avg = 0;
    printf("Student1: 810196684, Student2: 810196683\n\n");

    for (int i = 0; i < 5; i++) {
        solutions = 0;
        start_time = timeGetTime();
        solve(Queens);
        diff = timeGetTime() - start_time;
        printf("# solutions %d time: %ld \n", solutions, diff);
        avg += diff;
    }

    printf("# solutions %d avg time: %ld \n", solutions, avg/5);
    
    return 0;

}